#include <stdio.h>
#include <stdint.h>

int main() {

  uint32_t array1[3][5];  //2-D array of 4 byte (32 bits) unsigned integers
  uint32_t array2[3][5][7]; 

  int lenght; 
  printf("Here is how 2-D arrays are stored in computer memory\n");
  printf("Firstly, the array may be multi-dimensional but computer memory is not");
  printf("\nAlso, the compiler converts it to a 1-D array");

  int index = 0;
  int index2 = 0; 
  int index3 = 0; 
  int memoryArray1;
  int memoryArray2; 

  printf("\n\n");

  for (index = 0; index < 3; index++){
    for(index2 = 0; index2 < 5; index2++){
      
    

      // printf("\narray1[%p][%p] --> ", &index, &index2);

      
      printf("array1[%d][%d] --> ", index,index2);

      printf("%p\n", &array1[index][index2]); 

     

    }
  }

  printf("\n\n\n");

  for(index = 0; index < 3; index++){
    for(index2 = 0; index2 < 5; index2++){
      for(index3 = 0; index3 < 7; index3++){
        
        printf("array1[%d][%d][%d] --> ", index,index2, index3);
        printf("%p\n", &array2[index][index2][index3]);

      }
    }
  }


  printf("As we could see here the memory address is incrementing by 4 each time becuase it is a 32 bit (4 bytes) integer.\n\n(Goes from 0 --> 4 --> 8 --> c (which is equivelent to 12 in hexadecimal)");

  printf("\n\nHexadecimal is another base for counting it goes 1 - 9 and when it reaches 10-15 it goes to a-f "); 

  printf("\n\nIt matters not how many dimensional it is the compiler will always treat it as a 1-D array"); 


}
