#!/bin/bash

for ((i = 256; i <= 2048; i = i + 256))
do

	echo " "
	
	
	echo "EXECUTING ALGORITHMS 1 INPUT SIZE $i"
	./matrix_math 1 $i
	
	echo " "
	
	echo "EXECUTING ALGORITHMS 2 INPUT SIZE: $i"
	./matrix_math 2 $i


done 
