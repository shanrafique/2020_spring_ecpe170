#!/usr/bin/env python3

# Python DNS query client
#
# Example usage:
#   ./dns.py --type=A --name=www.pacific.edu --server=8.8.8.8
#   ./dns.py --type=AAAA --name=www.google.com --server=8.8.8.8

# Should provide equivalent results to:
#   dig www.pacific.edu A @8.8.8.8 +noedns
#   dig www.google.com AAAA @8.8.8.8 +noedns
#   (note that the +noedns option is used to disable the pseduo-OPT
#    header that dig adds. Our Python DNS client does not need
#    to produce that optional, more modern header)


from dns_tools import dns  # Custom module for boilerplate code

import argparse
import ctypes
import random
import socket
import struct
import sys


class CustomStruct (ctypes.BigEndianStructure):
	__fields__ = [
		("QR", ctypes.c_uint16, 1),	#1 bit field Most significant bit
		("OPCODE", ctypes.c_uint16, 4),
		("AA", ctypes.c_uint16, 1),
		("TC", ctypes.c_uint16, 1),
		("RD",ctypes.c_uint16, 1),
		("RA", ctypes.c_uint16, 1),
		("Reserved", ctypes.c_uint16, 3),
		("RCODE", ctypes.c_uint16, 4)
		
	
	
	]





def main():

    # Setup configuration
    parser = argparse.ArgumentParser(description='DNS client for ECPE 170')
    parser.add_argument('--type', action='store', dest='qtype',
                        required=True, help='Query Type (A or AAAA)')
    parser.add_argument('--name', action='store', dest='qname',
                        required=True, help='Query Name')
    parser.add_argument('--server', action='store', dest='server_ip',
                        required=True, help='DNS Server IP')

    args = parser.parse_args()
    qtype = args.qtype
    qname = args.qname
    server_ip = args.server_ip
    port = 53
    server_address = (server_ip, port)
    
    print("Port: ", port)

    if qtype not in ("A", "AAAA"):
        print("Error: Query Type must be 'A' (IPv4) or 'AAAA' (IPv6)")
        sys.exit()

    # Create UDP socket
    # ---------
    # STUDENT TO-DO
    # ---------

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


    # Generate DNS request message
    # ---------
    # STUDENT TO-DO
    # ---------
    
    raw_bytes = bytearray()
    parts = qname.split('.')
    
   
    raw_bytes.append(0x23)
    raw_bytes.append(0x02)
    
   
    raw_bytes.append(0x01)
    raw_bytes.append(0x20)
    
   
    raw_bytes.append(0x00)
    raw_bytes.append(0x01)
    
   
    raw_bytes.append(0x00)
    raw_bytes.append(0x00)
    
   
    raw_bytes.append(0x00)
    raw_bytes.append(0x00)
    
   
    raw_bytes.append(0x00)
    raw_bytes.append(0x00)
    
    
    for part in parts:
      raw_bytes.append(len(part))
      raw_bytes += bytes(part,'ascii')
      
    raw_bytes.append(0x00)
     
    if (qtype == 'A'):
       raw_bytes.append(0x00)
       raw_bytes.append(0x01)
    elif (qtype == 'AAAA'):
       raw_bytes.append(0x00)
       raw_bytes.append(0x1c)
       
     #class
    raw_bytes.append(0x00)
    raw_bytes.append(0x00)
     
    print(raw_bytes)
    
    
    
    
      
    
    
    
    


    # Send request message to server
    # (Tip: Use sendto() function for UDP)
    # ---------
    # STUDENT TO-DO
    # ---------


    sent_bytes = s.sendto(raw_bytes,server_address)

    # Receive message from server
    # (Tip: use recvfrom() function for UDP)
    # ---------
    # STUDENT TO-DO
    # ---------
    
    max_bytes = 4096
    (raw_bytes, server_address) = s.recvfrom(max_bytes)


    # Close socket
    # ---------
    # STUDENT TO-DO
    # ---------
    s.close()

    # Decode DNS message and display to screen
    dns.decode_dns(raw_bytes)


if __name__ == "__main__":
    sys.exit(main())
