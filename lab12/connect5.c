#include "connect5.h"


int userInput() {
	int droppingInColumn;
	printf("Enter a column: ");
	scanf("%d", &droppingInColumn);

	while(droppingInColumn < 1 || 7 < droppingInColumn) {
		printf("NOT A VALID INPUT PLEASE ENTER A VALID INPUT 1 THROUGH 7 only ");
		scanf("%d", &droppingInColumn);
	}
	if(*gridBoard[0][droppingInColumn] != *empty) {
		printf("FULL COLUMN!! Please choose new column\n");
		droppingInColumn = userInput();
	}
	return droppingInColumn;
}

int diagonal1() {
	int straight5 = 0;
	int inRow2 = 0;
	int inColumnAll = 0;
	for (int i = 0; i < 2; i++) {
		for(int r=0; r<rows; r++) {
			for(int c=0; c<cols; c++) {
				while(r+inColumnAll>=0 && c+inRow2<cols && *gridBoard[r+inColumnAll][c+inRow2] == *playerComputersChip[i]) {
					straight5++;
					if(straight5 == 5) {
						return i;
					}
					inRow2++;
					inColumnAll--;
				}
				straight5 = 0;
				inRow2 = 0;
				inColumnAll = 0;
			}
		}
	}
	return 3;
}

uint32_t get_random() {
	m_z = 36969 * (m_z & 65535) + (m_z >> 16);
	m_w = 18000 * (m_w & 65535) + (m_w >> 16);
	int randomNumber = (m_z << 16) + m_w;
	if ( randomNumber < 0) randomNumber*=-1;
	return randomNumber; /* 32-bit result */
}

int horizontal() {
  int straight5 = 0;
	int inRow2 = 0;
	for (int i = 0; i < 2; i++) {
		for(int r=0; r<rows; r++) {
			for(int c=0; c<cols; c++) {
				while(r<rows && c+inRow2<cols && *gridBoard[r][c+inRow2] == *playerComputersChip[i]) {
					straight5++;
					if(straight5 == 5) {
						return i;
					}
					inRow2++;
				}
				straight5 = 0;
				inRow2 = 0;
			}
		}
	}
	return 3;
}

void computerInput(char *ComputersChip, int usersChoice) {
	int inRow = 0;
	if(*gridBoard[inRow][usersChoice] == *empty) {
		gridBoard[inRow][usersChoice] = ComputersChip;
		while(inRow+1<rows && *gridBoard[inRow+1][usersChoice] == *empty) {
			gridBoard[inRow][usersChoice] = empty;
			gridBoard[inRow+1][usersChoice] = ComputersChip;
			inRow++;
		}
		inRow=0;
	}
}

void printgridBoard() {
	printf("    1  2  3  4  5  6  7\n");
	printf(" -------------------------\n");
	for(int r=0; r<rows; r++) {
		for(int c=0; c<cols; c++) {
			printf(" %c ", *gridBoard[r][c]);
		}
		printf("\n");
	}
	printf("\n");
}

int gamePlay() {
	int filled = 0;
	int state = 0;
	for (int i = 0; i < 2; i++) {
		for(int r=0; r<rows; r++) {
			for(int c=0; c<cols; c++) {
				if(*gridBoard[r][c] != *empty) {
					filled++;
        }
				state = horizontal();
				if (state != 3) return state;
				state = verticel();
				if (state != 3) return state;
				state = diagonal1();
				if (state != 3) return state;
        state = diagonal2();
				if (state != 3)	return state;
			}
		}
		if(filled >= (rows*cols)) return 2;
		else filled = 0;
	}
	return 3;
}
int diagonal2() {
	int straight5 = 0;
	int inRow2 = 0;
	int inColumnAll = 0;
	for (int i = 0; i < 2; i++) {
		for(int r=0; r<rows; r++) {
			for(int c=0; c<cols; c++) {
				while(r+inColumnAll>=0 && c+inRow2>=0 && *gridBoard[r+inColumnAll][c+inRow2] == *playerComputersChip[i]) {
					straight5++;
					if(straight5 == 5) {
						return i;
					}
					inRow2--;
					inColumnAll--;
				}
				straight5 = 0;
				inRow2 = 0;
				inColumnAll = 0;
			}
		}
	}
	return 3;
}
int verticel() {
	int straight5 = 0;
	int inColumnAll = 0;
	for (int i = 0; i < 2; i++) {
		for(int r=0; r<rows; r++) {
			for(int c=0; c<cols; c++) {
				while(r+inColumnAll<rows && c<cols && *gridBoard[r+inColumnAll][c] == *playerComputersChip[i]) {
					straight5++;
					if(straight5 == 5) {
						return i;
					}
					inColumnAll++;
				}
				straight5 = 0;
				inColumnAll = 0;
			}
		}
	}
	return 3;
}
int main(void) {
  printf("Please enter 2 numbers \n");
	printf("Number 1: ");
	scanf("%d", &m_w);
	printf("Number 2: ");
	scanf("%d", &m_z);
  printf("Coin toss ... ");
	Flip = get_random()%2;
	printf("%s goes first.\n", player[Flip]);
	
  while(status == 3) {
    printgridBoard();
		if(Flip == 0) {
			computerInput(player[Flip], userInput());
		}
		else { 
			Lost = (get_random()%7) + 1;
			while(*gridBoard[0][Lost] != *empty) {
				Lost = (get_random()%7) + 1;
			}
			computerInput(player[Flip], Lost);
		}
    status = gamePlay();
    Flip++;
		Flip = Flip%2;
  }
  printgridBoard();
	if (status == 0) printf("You Won!");
	else if (status == 1) printf("You lose!!");
	else printf("Tie!!!");
  return 0;
}
