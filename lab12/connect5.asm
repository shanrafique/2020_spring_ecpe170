.text
.globl main







main:

			#random genertor
			lw $s6, gameWon
				li $v0, 4
				la $a0, mainMSGpt1
				syscall
			
			# prints number1
				li $v0, 4
				la $a0, mainMSGpt2
				syscall
			
			# user input
				li $v0, 5
				syscall
				sw $v0, m_w 						
			
			# prints number2
				li $v0, 4
				la $a0, mainMSGpt3
				syscall
			
			# gets userinput number2
				li $v0, 5
				syscall
				sw $v0, m_z	

			# prints coin toss
				li $v0, 4
				la $a0, mainMSGpt4
				syscall
				
				#get_random function
				li $s5, 2
				jal get_random
				div $v0, $s5
				mfhi $s5			

        		#doing the modulus
				mul $s3, $s5, 2
				la $s4, player
				add $s3, $s3, $s4	
				
			# printing the modulus results
				li $v0, 4
				add $a0, $zero, $s3
				syscall
				
				# prints whoever goes first
				li $v0, 4
				la $a0, mainMSGpt5
				syscall
				
				
		forLop:	bne $s6, 3, vitality
				#print board
				jal printBoard 		# prints Board
				
				#// Player inserts chip
		ifHumansTurn:
				bne $s5, 0, notHumansTurn
				#get user_input
				jal user_input		# get User Input
				add $s0, $zero, $v0	# what user inputted is saved in $s0
				#insert human chip
				la $a0, humanChip
				add $a1, $zero, $s0
				jal insertChip		
		notHumansTurn:
				
		ifComputersTurn:
				bne $s5, 1, notComputersTurn
				#insert computers chip
				#acquire get_random()%7
				li $s7, 7
				jal get_random
				div $v0, $s7
				mfhi $s7			# get_random()%7
				add $s7, $s7, 1			# get_random()%7 + 1
		
		whle:
				# get *board[0][computersCol]
				la $t0, board
				mul $s8, $s7, 2
				add $s8, $s8, $t0	
				lb $s8, ($s8)	
				lb $t1, empty
				beq $s8, $t1, endWhle
				#acquire get_random()%7
				li $s7, 7
				jal get_random
				div $v0, $s7
				mfhi $s7			# get_random()%7
				add $s7, $s7, 1		# get_random()%7 + 1
				j whle
		endWhle:
				
				
				
				
				
				
				
				
				
				la $a0, computerChip
				add $a1, $zero, $s7
				jal insertChip		# insert chip function

		notComputersTurn:
				
				#checkBoard
				jal checkBoard		# checks Board
				add $s6, $zero, $v0
				

				add $s5, $s5, 1
				li $t5, 2
				div $s5, $t5
				mfhi $s5
				j forLop
		vitality:







				
				
				
				
				
			
			
				jal printBoard 		# print Board
				
				bne $s6, 0, firstif
				# print("Human won!\n")
				li $v0, 4
				la $a0, mainMSGpt6
				syscall
		firstif:
				bne $s6, 1, secondif
				# print("Computer won!\n")
				li $v0, 4
				la $a0, mainMSGpt7
				syscall
		secondif:
				bne $s6, 2, thirdif
				# print("Tie!\n")
				li $v0, 4
				la $a0, mainMSGpt8
				syscall
		thirdif:
				
				
				
					
				# Exits
				li $v0, 10
				syscall

.globl function
function: 


	# pushing
	subu $sp, $sp, 68
	sw $ra, ($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)
	sw $s4, 20($sp)
	sw $s5, 24($sp)
	sw $s6, 28($sp)
	sw $s7, 32($sp)
	sw $t0, 36($sp)
	sw $t1, 40($sp)
	sw $t2, 44($sp)
	sw $t3, 48($sp)
	sw $t4, 52($sp)
	sw $t5, 56($sp)
	sw $t6, 60($sp)
	sw $t7, 64($sp)
		
	
		
	# poping
	lw $ra, ($sp)
	lw $s0, 4($sp)
	lw $s1, 8($sp)
	lw $s2, 12($sp)
	lw $s3, 16($sp)
	lw $s4, 20($sp)
	lw $s5, 24($sp)
	lw $s6, 28($sp)
	lw $s7, 32($sp)
	lw $t0, 36($sp)
	lw $t1, 40($sp)
	lw $t2, 44($sp)
	lw $t3, 48($sp)
	lw $t4, 52($sp)
	lw $t5, 56($sp)
	lw $t6, 60($sp)
	lw $t7, 64($sp)
	addu $sp, $sp, 68 
	jr $ra








.globl printCheckPoint
printCheckPoint: 
	# pushing
	subu $sp, $sp, 84
	sw $ra, ($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)
	sw $s4, 20($sp)
	sw $s5, 24($sp)
	sw $s6, 28($sp)
	sw $s7, 32($sp)
	sw $t0, 36($sp)
	sw $t1, 40($sp)
	sw $t2, 44($sp)
	sw $t3, 48($sp)
	sw $t4, 52($sp)
	sw $t5, 56($sp)
	sw $t6, 60($sp)
	sw $t7, 64($sp)
	sw $a0, 68($sp)
	sw $a1, 72($sp)
	sw $v0, 76($sp)
	sw $v1, 80($sp)
		
	# algorithm goes here
	# print("Check point reached!!\n")
	li $v0, 4
	la $a0, checkPoint
	syscall
		
	# puts data from stack back to register (POP)
	lw $ra, ($sp)
	lw $s0, 4($sp)
	lw $s1, 8($sp)
	lw $s2, 12($sp)
	lw $s3, 16($sp)
	lw $s4, 20($sp)
	lw $s5, 24($sp)
	lw $s6, 28($sp)
	lw $s7, 32($sp)
	lw $t0, 36($sp)
	lw $t1, 40($sp)
	lw $t2, 44($sp)
	lw $t3, 48($sp)
	lw $t4, 52($sp)
	lw $t5, 56($sp)
	lw $t6, 60($sp)
	lw $t7, 64($sp)
	lw $a0, 68($sp)
	lw $a1, 72($sp)
	lw $v0, 76($sp)
	lw $v1, 80($sp)
	addu $sp, $sp, 84 
	jr $ra




#getRandom number
.globl get_random
get_random: # get_random()
	li $t0, 36969
	li $t1, 18000
	li $t2, 65535
	
	lw $t7, m_z
	lw $t6, m_w
	
	and $t3, $t7, $t2 
	mul $t3, $t0, $t3
	srl $t4, $t7, 16
	addu $t7, $t3, $t4
	sw $t7, m_z
	
	and $t3, $t6, $t2
	mul $t3, $t1, $t3
	srl $t4, $t6, 16
	addu $t6, $t3, $t4
	sw $t6, m_w
	
	sll $t3, $t7, 16
	addu $t3, $t3, $t6
	
	bgt $t3, $zero, endIfGR
	mul $t3, $t3, -1
	endIfGR:
	
	addu $v0, $t3,  $zero
	j $ra












.globl printBoard
printBoard: 
		# push
		subu $sp, $sp, 68
		sw $ra, ($sp)
		sw $s0, 4($sp)
		sw $s1, 8($sp)
		sw $s2, 12($sp)
		sw $s3, 16($sp)
		sw $s4, 20($sp)
		sw $s5, 24($sp)
		sw $s6, 28($sp)
		sw $s7, 32($sp)
		sw $t0, 36($sp)
		sw $t1, 40($sp)
		sw $t2, 44($sp)
		sw $t3, 48($sp)
		sw $t4, 52($sp)
		sw $t5, 56($sp)
		sw $t6, 60($sp)
		sw $t7, 64($sp)
			
	# set registers values
		lw $s1, board_rows				
		lw $s3, board_cols				
		la $s6, board				
		lw $s7, DATA_SIZE
		
	# print("    1  2  3  4  5  6  7\n ---+--+--+--+--+--+--+---\n");
		li $v0, 4
		la $a0, boardMarkers
		syscall
		
	# start for-loop 
	li $s0, 0				# int r = 0
	startForLoopRow:
		bge $s0, $s1, endForLoopRow	# if!(r<board_rows)
		
	# start for-loop for(int c=0; c<board_cols; c++)
	li $s2, 0				# int c = 0
	startForLoopCol:
		bge $s2, $s3, endForLoopCol	
		
	# print(" ")
		li $v0, 4
		la $a0, space
		syscall
		
	# prints gridboard[r][c]
		mul $s4, $s0, $s3	
		add $s4, $s4, $s2
		mul $s4, $s4, $s7	
		add $s4, $s4, $s6	
		
	# print letter
		li $v0, 4
		la $a0, ($s4)
		syscall
		
		
	# print space
		li $v0, 4
		la $a0, space
		syscall
		
		addi $s2, $s2, 1		# c++
		j startForLoopCol
	endForLoopCol:
	
	# print("\n")
		li $v0, 4
		la $a0, newLine
		syscall
		
		addi $s0, $s0, 1		# r++
		j startForLoopRow
	endForLoopRow:
	
	
	# new line char
		li $v0, 4
		la $a0, newLine
		syscall
	
	
	
			
		# op
		lw $ra, ($sp)
		lw $s0, 4($sp)
		lw $s1, 8($sp)
		lw $s2, 12($sp)
		lw $s3, 16($sp)
		lw $s4, 20($sp)
		lw $s5, 24($sp)
		lw $s6, 28($sp)
		lw $s7, 32($sp)
		lw $t0, 36($sp)
		lw $t1, 40($sp)
		lw $t2, 44($sp)
		lw $t3, 48($sp)
		lw $t4, 52($sp)
		lw $t5, 56($sp)
		lw $t6, 60($sp)
		lw $t7, 64($sp)
		addu $sp, $sp, 68 
		jr $ra





.globl user_input
user_input: 
		# pushing
		subu $sp, $sp, 68
		sw $ra, ($sp)
		sw $s0, 4($sp)
		sw $s1, 8($sp)
		sw $s2, 12($sp)
		sw $s3, 16($sp)
		sw $s4, 20($sp)
		sw $s5, 24($sp)
		sw $s6, 28($sp)
		sw $s7, 32($sp)
		sw $t0, 36($sp)
		sw $t1, 40($sp)
		sw $t2, 44($sp)
		sw $t3, 48($sp)
		sw $t4, 52($sp)
		sw $t5, 56($sp)
		sw $t6, 60($sp)
		sw $t7, 64($sp)
			
		li $s0, 99999					
		lw $s1, board_rows	
		lw $s3, board_cols			
		la $s6, board			
		lw $s7, DATA_SIZE				
		lb $s2, empty					
		
	# print enter column for chip
		li $v0, 4
		la $a0, userIP
		syscall
		
	# user input
		li $v0, 5
		syscall
		move $s1, $v0 			# store result in $s1
		
	
	checkWhileConditionIP:
		blt $s1, 1, startWhileLoopIP		
		bgt $s1, 7, startWhileLoopIP		
		j endWhileLoopIP
		
		
		
		
		
		
		
		
		
	startWhileLoopIP:					
	# print enter valid  key please
		li $v0, 4
		la $a0, invalidIP
		syscall
		
	# et user input
		li $v0, 5
		syscall
		add $s1, $zero, $v0 		# store result in $s1
		j checkWhileConditionIP
		
		
		
		
		
	endWhileLoopIP:
		mul $s4, $zero, $s3	
		add $s4, $s4, $s1	
		mul $s4, $s4, $s7	
		add $s4, $s4, $s6	
		lb $s5, ($s4)	
	
		beq $s2, $s5, endIfIP
		
		
		
	# prints coumn is full
		li $v0, 4
		la $a0, colFullP
		syscall
		jal user_input
		add $s1, $zero, $v0	
	endIfIP:
	
		add $v0, $zero, $s1
			
		# popping
		lw $ra, ($sp)
		lw $s0, 4($sp)
		lw $s1, 8($sp)
		lw $s2, 12($sp)
		lw $s3, 16($sp)
		lw $s4, 20($sp)
		lw $s5, 24($sp)
		lw $s6, 28($sp)
		lw $s7, 32($sp)
		lw $t0, 36($sp)
		lw $t1, 40($sp)
		lw $t2, 44($sp)
		lw $t3, 48($sp)
		lw $t4, 52($sp)
		lw $t5, 56($sp)
		lw $t6, 60($sp)
		lw $t7, 64($sp)
		addu $sp, $sp, 68 
		jr $ra




.globl insertChip
insertChip: 
		# sace s into stack
		subu $sp, $sp, 68
		sw $ra, ($sp)
		sw $s0, 4($sp)
		sw $s1, 8($sp)
		sw $s2, 12($sp)
		sw $s3, 16($sp)
		sw $s4, 20($sp)
		sw $s5, 24($sp)
		sw $s6, 28($sp)
		sw $s7, 32($sp)
		sw $t0, 36($sp)
		sw $t1, 40($sp)
		sw $t2, 44($sp)
		sw $t3, 48($sp)
		sw $t4, 52($sp)
		sw $t5, 56($sp)
		sw $t6, 60($sp)
		sw $t7, 64($sp)
		
	# initializing variables
		lb $s0, ($a0)				
		add $s1, $zero, $a1			
		la $s2, board					
		lw $s3, board_rows				
		lw $s4, board_cols				
		add $s5, $zero, 0				
		lb $s6, empty					
		lw $s7, DATA_SIZE			
		
	# gridboard function 
		mul $t0, $s5, $s4			
		add $t0, $t0, $s1				
		mul $t0, $t0, $s7				
		add $t0, $t0, $s2				
		lb $t1, ($t0)	
		
		
		
		
		
		
					
		
	ifIC:
		bne $t1, $s6, endIfIC	
		sb $s0, ($t0)
		
		
		
		
		
	whileIC:
	#grid board fucniton 
		mul $t0, $s5, $s4				
		add $t0, $t0, $s1				
		mul $t0, $t0, $s7				
		add $t0, $t0, $s2				
		lb $t1, ($t0)				
		
		add $t2, $s5, 1				
		mul $t5, $s4, 2
		add $t3, $t0, $t5				
		lb $t4, ($t3)					
		
		bge $t2, $s3, endWhileIC		
		bne $t4, $s6, endWhileIC		
		
		
		#grid board print
		mul $t0, $s5, $s4				
		add $t0, $t0, $s1				
		mul $t0, $t0, $s7				
		add $t0, $t0, $s2				
		sb $s6, ($t0)

		add $t2, $s5, 1					
		mul $t5, $s4, 2
		add $t3, $t0, $t5				
		lb $t4, ($t3)					
		sb $s0, ($t3)					
		
		add $s5, $s5, 1					
	
	
	
		j whileIC
	endWhileIC:
		li $s5, 0						
	endIfIC:
		
			
		# puts data from stack back to register (POP)
		lw $ra, ($sp)
		lw $s0, 4($sp)
		lw $s1, 8($sp)
		lw $s2, 12($sp)
		lw $s3, 16($sp)
		lw $s4, 20($sp)
		lw $s5, 24($sp)
		lw $s6, 28($sp)
		lw $s7, 32($sp)
		lw $t0, 36($sp)
		lw $t1, 40($sp)
		lw $t2, 44($sp)
		lw $t3, 48($sp)
		lw $t4, 52($sp)
		lw $t5, 56($sp)
		lw $t6, 60($sp)
		lw $t7, 64($sp)
		addu $sp, $sp, 68 
		jr $ra













.globl checkBoard
checkBoard: 
			# pushing
			subu $sp, $sp, 80
			sw $ra, ($sp)
			sw $s0, 4($sp)
			sw $s1, 8($sp)
			sw $s2, 12($sp)
			sw $s3, 16($sp)
			sw $s4, 20($sp)
			sw $s5, 24($sp)
			sw $s6, 28($sp)
			sw $s7, 32($sp)
			sw $s8, 36($sp)
			sw $t0, 40($sp)
			sw $t1, 44($sp)
			sw $t2, 48($sp)
			sw $t3, 52($sp)
			sw $t4, 56($sp)
			sw $t5, 60($sp)
			sw $t6, 64($sp)
			sw $t7, 68($sp)
			sw $t8, 72($sp)
			sw $t9, 76($sp)
			
			# declare variables
			lw $t0, board_rows
			lw $t1, board_cols
			lb $t2, empty
			lw $t3, DATA_SIZE
			la $t4, board
			li $s0, 0						
			li $s1, 0		 	
			
			li $s2, 0	
	forLoopCBI:
			bge $s2, 2, endForLoopCBI		# !(i < 2)
	
			li $s3, 0
			
			
					 
	forLoopCBR:
			bge $s3, $t0, endForLoopCBR		
			
			li $s4, 0
			
			
			
	forLoopCBC:
			bge $s4, $t1, endForLoopCBC
			



	ifFull:
		# board[row][col]
			mul $s5, $s3, $t1				
			add $s5, $s5, $s4			
			mul $s5, $s5, $t3				
			add $s5, $s5, $t4				
			lb $s6, ($s5)					
	
			beq $s6, $t2, endIfEmpty
			add $s0, $s0, 1	
			
			
	endIfEmpty:
			
			jal checkHorizontal
			add $s1, $zero, $v0
	ifH:
			beq $s1, 3, endIfH		
			add $v0, $zero, $s1				
			j return
	endIfH:
	
			jal checkVertical
			add $s1, $zero, $v0
	ifC:
			beq $s1, 3, endIfC			
			add $v0, $zero, $s1		
			j return
	endIfC:
	
			jal checkDiagnalUp
			add $s1, $zero, $v0
	ifDU:
			beq $s1, 3, endIfDU				
			add $v0, $zero, $s1				
			j return
	endIfDU:
		
			jal checkDiagnalDown
			add $s1, $zero, $v0
	ifDD:
			beq $s1, 3, endIfDD				
			add $v0, $zero, $s1		
			j return
	endIfDD:
			



		
			
			add $s4, $s4, 1		# c++
			j forLoopCBC
	endForLoopCBC:
	
			add $s3, $s3, 1		# r++
			j forLoopCBR
	endForLoopCBR:
	
			add $s2, $s2, 1		# i++
			j forLoopCBI
	endForLoopCBI:

	
			add $v0, $zero, 3	# return 3
				
				
				
				
	return:
			# pops
			lw $ra, ($sp)
			lw $s0, 4($sp)
			lw $s1, 8($sp)
			lw $s2, 12($sp)
			lw $s3, 16($sp)
			lw $s4, 20($sp)
			lw $s5, 24($sp)
			lw $s6, 28($sp)
			lw $s7, 32($sp)
			lw $s8, 36($sp)
			lw $t0, 40($sp)
			lw $t1, 44($sp)
			lw $t2, 48($sp)
			lw $t3, 52($sp)
			lw $t4, 56($sp)
			lw $t5, 60($sp)
			lw $t6, 64($sp)
			lw $t7, 68($sp)
			lw $t8, 72($sp)
			lw $t9, 76($sp)
			addu $sp, $sp, 80 
			jr $ra








.globl checkHorizontal
checkHorizontal: 
			# push
			subu $sp, $sp, 80
			sw $ra, ($sp)
			sw $s0, 4($sp)
			sw $s1, 8($sp)
			sw $s2, 12($sp)
			sw $s3, 16($sp)
			sw $s4, 20($sp)
			sw $s5, 24($sp)
			sw $s6, 28($sp)
			sw $s7, 32($sp)
			sw $s8, 36($sp)
			sw $t0, 40($sp)
			sw $t1, 44($sp)
			sw $t2, 48($sp)
			sw $t3, 52($sp)
			sw $t4, 56($sp)
			sw $t5, 60($sp)
			sw $t6, 64($sp)
			sw $t7, 68($sp)
			sw $t8, 72($sp)
			sw $t9, 76($sp)
				
				
				
			li $s0, 0
			li $s1, 0	
			li $s2, 0		
			lw $t0, board_rows
			lw $t1, board_cols
			lb $t2, empty
			lw $t3, DATA_SIZE
			la $t4, board
			
					
			li $s5, 0
			
			
			
	forLoopCHI:
			bge $s5, 2, endForLoopCHI	
	
			li $s3, 0		# s3 (r) = 0
	forLoopCHR:
			bge $s3, $t0, endForLoopCHR	
			
			li $s4, 0
	forLoopCHC:
			bge $s4, $t1, endForLoopCHC
			



	whileLoopCH:
			bge $s3, $t0, endWhileLoopCH
			add $t5, $s4, $s1		# c+horizonatal
			bge $t5, $t1, endWhileLoopCH
			
			
		# &board[r][c]	
			mul $s6, $s3, $t1		# rowIndex * colSize
			add $t8, $s4, $s1
			add $s6, $s6, $t8		# + colIndex
			mul $s6, $s6, $t3		
			add $s6, $s6, $t4		# + baseAddress
			lb $s7, ($s6)			
			
			mul $t6, $s5, $t3		
			la $t7, playerChip		
			add $t6, $t7, $t6		# &playerChip + index
			lb $s8, ($t6)		
			
			bne $s7, $s8, endWhileLoopCH	
			
			add $s0, $s0, 1
	ifCH:
			bne $s0, 5, endIfCH		# !(chipsConnected == 5)
			add $v0, $zero, $s5		# return i
			j returnCH
			
	endIfCH:
			add $s1, $s1, 1			# addHori++;
	
			j whileLoopCH
	endWhileLoopCH:
	
			li $s0, 0	
			li $s1, 0	# $s1 = horizontal = 0
			li $s2, 0	# $s2 = vertical = 0
			
# finish				
			add $s4, $s4, 1	# c++
			j forLoopCHC
	endForLoopCHC:
		
			add $s3, $s3, 1	# r++
			j forLoopCHR
	endForLoopCHR:
	
			add $s5, $s5, 1	# i++
			j forLoopCHI
	endForLoopCHI:
			
			li $v0, 3	# return 3
	returnCH:
				
			# puts data from stack back to register (POP)
			lw $ra, ($sp)
			lw $s0, 4($sp)
			lw $s1, 8($sp)
			lw $s2, 12($sp)
			lw $s3, 16($sp)
			lw $s4, 20($sp)
			lw $s5, 24($sp)
			lw $s6, 28($sp)
			lw $s7, 32($sp)
			lw $s8, 36($sp)
			lw $t0, 40($sp)
			lw $t1, 44($sp)
			lw $t2, 48($sp)
			lw $t3, 52($sp)
			lw $t4, 56($sp)
			lw $t5, 60($sp)
			lw $t6, 64($sp)
			lw $t7, 68($sp)
			lw $t8, 72($sp)
			lw $t9, 76($sp)
			addu $sp, $sp, 80 
			jr $ra











.globl checkVertical
checkVertical: 
			# saves s registers by pushing it into the stack
			subu $sp, $sp, 80
			sw $ra, ($sp)
			sw $s0, 4($sp)
			sw $s1, 8($sp)
			sw $s2, 12($sp)
			sw $s3, 16($sp)
			sw $s4, 20($sp)
			sw $s5, 24($sp)
			sw $s6, 28($sp)
			sw $s7, 32($sp)
			sw $s8, 36($sp)
			sw $t0, 40($sp)
			sw $t1, 44($sp)
			sw $t2, 48($sp)
			sw $t3, 52($sp)
			sw $t4, 56($sp)
			sw $t5, 60($sp)
			sw $t6, 64($sp)
			sw $t7, 68($sp)
			sw $t8, 72($sp)
			sw $t9, 76($sp)
				
			# initializing variables
			li $s0, 0			# $s0 = int 0
			li $s1, 0			# $s1 = int = 0
			li $s2, 0			# $s2 = int = 0
			lw $t0, board_rows
			lw $t1, board_cols
			la $t2, board
			lw $t3, DATA_SIZE
			
			
						
			li $s5, 0			# $s5 = int i= 0
	forLoopCVI:
			bge $s5, 2, endForLoopCVI
	
			li $s3, 0			# $s3 = int r= 0
	forLoopCVR:
			bge $s3, $t0, endForLoopCVR		
			
			li $s4, 0			# $s4 = int c = 0
	forLoopCVC:
			bge $s4, $t1, endForLoopCVC		
			
# start
	
	whileLoopCV:
			add $t4, $s3, $s2		#add to vertical
			bge $t4, $t0, endWhileLoopCV	
			bge $s4, $t1, endWhileLoopCV
			
			
			
		# &board[r][c]	
			mul $s6, $t4, $t1				
			add $s6, $s6, $s4		# + colIndex
			mul $s6, $s6, $t3				
			add $s6, $s6, $t2		# + baseAddress
			lb $s7, ($s6)					
			
			mul $t6, $s5, $t3			
			la $t7, playerChip		
			add $t6, $t7, $t6				
			lb $s8, ($t6)			# playerChip[index]
			
			bne $s7, $s8, endWhileLoopCV	 
			
			add $s0, $s0, 1			
	ifCV:
			bne $s0, 5, endIfCV	# !(chipsConnected == 5)
			add $v0, $zero, $s5	# return i
			j returnCV
			
	endIfCV:
			add $s2, $s2, 1		# addVert++;
	
			j whileLoopCV
	endWhileLoopCV:
	
			li $s0, 0		
			li $s1, 0		# $s1 = Hori = 0
			li $s2, 0		# $s2 = Vert = 0
			
# finish				
			add $s4, $s4, 1		# c++
			j forLoopCVC
	endForLoopCVC:
		
			add $s3, $s3, 1		# r++
			j forLoopCVR
	endForLoopCVR:
	
			add $s5, $s5, 1		# i++
			j forLoopCVI
	endForLoopCVI:
			
			li $v0, 3		# return 3
	returnCV:
				
			# puts data from stack back to register (POP)
			lw $ra, ($sp)
			lw $s0, 4($sp)
			lw $s1, 8($sp)
			lw $s2, 12($sp)
			lw $s3, 16($sp)
			lw $s4, 20($sp)
			lw $s5, 24($sp)
			lw $s6, 28($sp)
			lw $s7, 32($sp)
			lw $s8, 36($sp)
			lw $t0, 40($sp)
			lw $t1, 44($sp)
			lw $t2, 48($sp)
			lw $t3, 52($sp)
			lw $t4, 56($sp)
			lw $t5, 60($sp)
			lw $t6, 64($sp)
			lw $t7, 68($sp)
			lw $t8, 72($sp)
			lw $t9, 76($sp)
			addu $sp, $sp, 80 
			jr $ra













.globl checkDiagnalUp
checkDiagnalUp: 
			# pushing
			subu $sp, $sp, 80
			sw $ra, ($sp)
			sw $s0, 4($sp)
			sw $s1, 8($sp)
			sw $s2, 12($sp)
			sw $s3, 16($sp)
			sw $s4, 20($sp)
			sw $s5, 24($sp)
			sw $s6, 28($sp)
			sw $s7, 32($sp)
			sw $s8, 36($sp)
			sw $t0, 40($sp)
			sw $t1, 44($sp)
			sw $t2, 48($sp)
			sw $t3, 52($sp)
			sw $t4, 56($sp)
			sw $t5, 60($sp)
			sw $t6, 64($sp)
			sw $t7, 68($sp)
			sw $t8, 72($sp)
			sw $t9, 76($sp)
				
			# initializing variables
			li $s0, 0
			li $s1, 0		
			li $s2, 0		
			lw $t0, board_rows
			lw $t1, board_cols
			la $t2, board
			lw $t3, DATA_SIZE
			
			
						
			li $s5, 0
			
			
	forLoopCDUI:
			bge $s5, 2, endForLoopCDUI	# !(i < 2)
	
			li $s3, 0			# $s3 = int r
	forLoopCDUR:
			bge $s3, $t0, endForLoopCDUR	
			
			li $s4, 0			# $int c - 0
	forLoopCDUC:
			bge $s4, $t1, endForLoopCDUC
			
	
	whileLoopCDU:
			add $t4, $s3, $s2				
			blt $t4, $zero, endWhileLoopCDU	
			add $t5, $s4, $s1		# c+addHori
			bge $t5, $t1, endWhileLoopCDU	
		# &board[r][c]	
			mul $s6, $t4, $t1		# rowIndex * colSize
			add $s6, $s6, $t5			
			mul $s6, $s6, $t3				
			add $s6, $s6, $t2				
			lb $s7, ($s6)				
			
			mul $t6, $s5, $t3				
			la $t7, playerChip
			add $t6, $t7, $t6		
			lb $s8, ($t6)			 
			
			bne $s7, $s8, endWhileLoopCDU
			
			
			
			
			add $s0, $s0, 1			# chipsConnected++
	ifCDU:
			bne $s0, 5, endIfCDU		# !(chipsConnected == 5)
			add $v0, $zero, $s5				
			
								# return i
			j returnCDU
			
	endIfCDU:
			add $s2, $s2, -1		# addVert--;
			add $s1, $s1, 1			
	
			j whileLoopCDU
			
			
	endWhileLoopCDU:
	
			li $s0, 0		# $s0 = 0
			li $s1, 0		# $s1 = Hori = 0
			li $s2, 0		
			
# finish				
			add $s4, $s4, 1		# c++
			j forLoopCDUC
	endForLoopCDUC:
		
			add $s3, $s3, 1		# r++
			j forLoopCDUR
	endForLoopCDUR:
	
			add $s5, $s5, 1		# i++
			j forLoopCDUI
	endForLoopCDUI:
			
			li $v0, 3						# return 3
	returnCDU:
				
			# puts data from stack back to register (POP)
			lw $ra, ($sp)
			lw $s0, 4($sp)
			lw $s1, 8($sp)
			lw $s2, 12($sp)
			lw $s3, 16($sp)
			lw $s4, 20($sp)
			lw $s5, 24($sp)
			lw $s6, 28($sp)
			lw $s7, 32($sp)
			lw $s8, 36($sp)
			lw $t0, 40($sp)
			lw $t1, 44($sp)
			lw $t2, 48($sp)
			lw $t3, 52($sp)
			lw $t4, 56($sp)
			lw $t5, 60($sp)
			lw $t6, 64($sp)
			lw $t7, 68($sp)
			lw $t8, 72($sp)
			lw $t9, 76($sp)
			addu $sp, $sp, 80 
			jr $ra












.globl checkDiagnalDown
checkDiagnalDown: 
			# saves s registers by pushing it into the stack
			subu $sp, $sp, 80
			sw $ra, ($sp)
			sw $s0, 4($sp)
			sw $s1, 8($sp)
			sw $s2, 12($sp)
			sw $s3, 16($sp)
			sw $s4, 20($sp)
			sw $s5, 24($sp)
			sw $s6, 28($sp)
			sw $s7, 32($sp)
			sw $s8, 36($sp)
			sw $t0, 40($sp)
			sw $t1, 44($sp)
			sw $t2, 48($sp)
			sw $t3, 52($sp)
			sw $t4, 56($sp)
			sw $t5, 60($sp)
			sw $t6, 64($sp)
			sw $t7, 68($sp)
			sw $t8, 72($sp)
			sw $t9, 76($sp)
				
			# initializing variables
			li $s0, 0						# $s0 = int chipsConnected = 0
			li $s1, 0						# $s1 = int addHori = 0
			li $s2, 0						# $s2 = int addVert = 0
			lw $t0, board_rows
			lw $t1, board_cols
			la $t2, board
			lw $t3, DATA_SIZE
			
			
						
			li $s5, 0		# $s5 = int i = 0
	forLoopCDDI:
			bge $s5, 2, endForLoopCDDI		
	
			li $s3, 0		# $s3 = int r = 0
	forLoopCDDR:
			bge $s3, $t0, endForLoopCDDR	
			
			li $s4, 0	# $s4 = int c = 0
	forLoopCDDC:
			bge $s4, $t1, endForLoopCDDC
			




	
	whileLoopCDD:
			add $t4, $s3, $s2		# add to vertical
			blt $t4, $zero, endWhileLoopCDD	
			add $t5, $s4, $s1		# add to horizontal
			blt $t5, $zero, endWhileLoopCDD		
		# &board[r][c]	
			mul $s6, $t4, $t1		
			add $s6, $s6, $t5			
			mul $s6, $s6, $t3			
			add $s6, $s6, $t2	# + baseAddress
			lb $s7, ($s6)					
			
			mul $t6, $s5, $t3			
			la $t7, playerChip			
			add $t6, $t7, $t6		
			lb $s8, ($t6)					
			
			bne $s7, $s8, endWhileLoopCDD
			
			add $s0, $s0, 1
			
			
	ifCDD:
			bne $s0, 5, endIfCDD		
			add $v0, $zero, $s5	# return i
			j returnCDD
			
	endIfCDD:
			add $s2, $s2, -1	# aadding to veritical
			add $s1, $s1, -1	#adding to horizantol
	
			j whileLoopCDD
	endWhileLoopCDD:
	
			li $s0, 0		
			li $s1, 0		# $s1 = horizontal = 0
			li $s2, 0		# $s2 = vertical = 0
			
# finish				
			add $s4, $s4, 1		# c++
			j forLoopCDDC
	endForLoopCDDC:
		
			add $s3, $s3, 1		# r++
			j forLoopCDDR
	endForLoopCDDR:
	
			add $s5, $s5, 1		# i++
			j forLoopCDDI
	endForLoopCDDI:
			
			li $v0, 3		# return 3
	returnCDD:
				
			# puts data from stack back to register (POP)
			lw $ra, ($sp)
			lw $s0, 4($sp)
			lw $s1, 8($sp)
			lw $s2, 12($sp)
			lw $s3, 16($sp)
			lw $s4, 20($sp)
			lw $s5, 24($sp)
			lw $s6, 28($sp)
			lw $s7, 32($sp)
			lw $s8, 36($sp)
			lw $t0, 40($sp)
			lw $t1, 44($sp)
			lw $t2, 48($sp)
			lw $t3, 52($sp)
			lw $t4, 56($sp)
			lw $t5, 60($sp)
			lw $t6, 64($sp)
			lw $t7, 68($sp)
			lw $t8, 72($sp)
			lw $t9, 76($sp)
			addu $sp, $sp, 80 
			jr $ra
			
			
			
			
.data
	board:	.asciiz	"C", ".", ".", ".", ".", ".", ".", ".", "C"
					.asciiz	"H", ".", ".", ".", ".", ".", ".", ".", "H"
					.asciiz	"C", ".", ".", ".", ".", ".", ".", ".", "C"
					.asciiz	"H", ".", ".", ".", ".", ".", ".", ".", "H"
					.asciiz	"C", ".", ".", ".", ".", ".", ".", ".", "C"
					.asciiz	"H", ".", ".", ".", ".", ".", ".", ".", "H"
	DATA_SIZE:		.word 	2
	board_rows:		.word 	6
	gameWon:		.word	3
	board_cols:		.word 	9
	empty:			.asciiz	"."
	playerTurn:		.word	0
	
	m_w:			.word	123
	playerChip:		.asciiz "H", "C"
	m_z:			.word	456
	player:			.asciiz "HUMAN", "COMPUTER"
	computersCol:	.word	0
	# string for printBoard()
	boardMarkers:	.asciiz "    1  2  3  4  5  6  7\n -------------------------\n"
	
	# string for int user_input()
	invalidIP:		.asciiz "Enter a VALID column to insert chip: "
	colFullP:		.asciiz "That column is full.\n"
	userIP:			.asciiz "Enter a column to insert chip: "
	
	# string for int main()
	mainMSGpt2:		.asciiz "Number 1: "
	mainMSGpt3:		.asciiz "Number 2: "
	mainMSGpt5:		.asciiz " goes first.\n"
	mainMSGpt4:		.asciiz "Coin toss ... "
	mainMSGpt6:		.asciiz "You won!\n"
	mainMSGpt7:		.asciiz "Compute won! \n"
	mainMSGpt8:		.asciiz "Tie!\n"
	mainMSGpt1:		.asciiz "Enter two number \n"
	space:			.asciiz " "
	newLine:		.asciiz "\n"
	variable:		.asciiz "  dasfsadfa  "
	humanChip: 		.asciiz "H"
	computerChip:	.asciiz "C"
	checkPoint:		.asciiz "Check point reached!!\n"
