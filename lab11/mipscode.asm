.text
.globl main


#uint32_t m_w = 50000;
#uint32_t m_z = 60000;
#uint32_t random_in_range(uint32_t low, uint32_t high)
#{
#  uint32_t range = high-low+1;
#  uint32_t rand_num = get_random();
#  return (rand_num % range) + low;
#}

#// Generate random 32-bit unsigned number
#// based on multiply-with-carry method shown
#// at http://en.wikipedia.org/wiki/Random_number_generation

#uint32_t get_random()
#{
#  uint32_t result;
#  m_z = 36969 * (m_z & 65535) + (m_z >> 16);
#  m_w = 18000 * (m_w & 65535) + (m_w >> 16);
#  result = (m_z << 16) + m_w;  /* 32-bit result */
#  return result;
#}   
#int main()
#    {
#       uint32_t n1, n2;
    
#       int i=0;

#        for(i=0;i<10;i++) {   
#          n1=random_in_range(1,10000);
#	  n2=random_in_range(1,10000);
       
#          printf("\n G.C.D of %u and %u is %u.", n1, n2, gcd(n1,n2));
#	}
#       return 0;
#    }
    
#	uint32_t gcd(uint32_t n1, uint32_t n2)
#    {
#        if (n2 != 0)
#           return gcd(n2, n1%n2);
#        else 
#           return n1;
#    }




#In this program we will be converting some code given by 
#the professor into MIPS. This code finds the 
#GCD by using Euclidean algorithm.

#maps
# s0 = m_w
# s1 = m_z
#s2 = i (promarily putting its value to 0)
#s3 = 10

#depending what time im calling the function these values change
#a0 = num1
#a1 = num2
#a0 = 1
#a1=10000
#v's are for my return values 

main:

li $s3, 10		#s3 = int 10
addi $s0, $s0, 0	#s0 = m_w
addi $s1, $s1, 0 	#s1 = m_z
li $s2, 0 		# s2 (i) = 0


forloop:       
bge $s2, $s3 exit 	# i < 10go to exit

#just constants need it for our arugments 
li $a0, 1 		#a0 = int i
li $a1, 10000 		#a1 = int 1000
jal jumptorand	 	#we go to our funtion jumptorand with the above argumentd
move $s0, $v0            #move the return into s0
jal jumptorand 		#we go to our funtion jumptorand with the above 
move $s1, $v0 		# n1 (s1) = to whatever the return values are for jumptorand


move $a0, $s0		#argument for function
move $a1, $s1		#2nd argument for function
jal euclidean 		#calling the euclidean
move $s4, $v0   	#move the return values into s4 



#HERE I am printin the values (4 is the constant we to put into v0 to print)         
li $v0, 4 
la $a0, mssg1
syscall

#still print num1 (n1)          
li $v0, 1
move $a0, $s0
syscall

#prints the mssg2
li $v0, 4
la $a0, mssg2
syscall

 
#prints the 2nd number (n2)            
li $v0, 1 
move $a0, $s1
syscall


#pinrts =                       
li $v0, 4 
la $a0, mssg3
syscall


#here we are prints the result the GCD of num1 and num2
li $v0, 1 
move $a0, $s4
syscall
             
addi $s2, $s2, 1 #s2 (i) = se2 + 1
j forloop 	#jump back to forloop

             

exit:              
li $v0, 10	#exits the program 
syscall


#this part of the code is the gcd of the c code
#here we have if num2 does not equal 0 we will do recursion 
#if num2 does == 0 we will return num1

euclidean:      

#pushing (to store values onto stack)

addi $sp, $sp, -4	#adjusting the stack pointer	
sw $s0, 0($sp)		#save s0

addi $sp, $sp, -4	#adjusting the stack pointer
sw $s1, 0($sp)		#save s1
addi $sp, $sp, -4	#adjusting the stack pointer
sw $s2, 0($sp)		#save s2

addi $sp, $sp, -4	#adjusting the stack pointer
sw $s3, 0($sp) 		#save s3
addi $sp, $sp, -4	#adjusting the stack pointer
sw $ra, 0($sp)		#save ra (pc + 4)

                          

li $s0, 0			#load value 0 into s0
beq $a1, $s0, outsideIfloop	#if a1 == s0 jump to out side of if loop 
addi $sp, $sp, -4		#adjusting stack pointer

sw $a0, 0($sp)			#save a0
addi $sp, $sp, -4		#adjusting stack pointer
sw $a1, 0($sp)			#save a1

             

divu $a0, $a1		#dividing unsigned
mfhi $t0		#moving from igh to low
addiu $a0, $a1, 0	#adding unsign
addiu $a1, $t0, 0	#adding

jal euclidean		#go to function

lw $a0, 0($sp)		#loading word into a0
addi $sp, $sp, 4	#adjusting stack pointer
lw $a1, 0($sp)		#load into a1
addi $sp, $sp 4		#adjusting stack pointer
j outofgcd	#jumping 

             

outsideIfloop:     
addiu $v0, $a0, 0	#adding unsign integer

 
outofgcd:        
lw $ra, 0($sp)		#load into ra
addi $sp, $sp, 4	#adjusting stack pointer
lw $s3, 0($sp)		#load word into s3
addi $sp, $sp, 4	#adjusting stack pointer
lw $s2, 0($sp)		#load into s2
addi $sp, $sp, 4	#adjusting stack pointer
lw $s1, 0($sp)		#load word into s1

addi $sp, $sp, 4	#adjusting stack pointer
lw $s0, 0($sp)		#resotre into s0
addi $sp, $sp, 4	#adjusting stack pointer
jr $ra			#jump back to pc +4

             

jumptorand:

#pushing to save onto stack

addi $sp, $sp, -4 	#adjusting stack pointer
sw $s0, 0($sp)		#saving num1
addi $sp, $sp, -4 	#adjusting stack pointer
sw $s1, 0($sp)		#saving num2

addi $sp, $sp, -4 	#adjusting stack pointer
sw $s2, 0($sp)		#saving i
addi $sp, $sp, -4 	#adjusting stack pointer
sw $s3, 0($sp)		#saving s3 (10)
addi $sp, $sp, -4 	#adjusting stack pointer
sw $ra, 0($sp)		#saving ra (pc + 4)



addiu $s0, $a0, 1 #adding 1
subu  $s1, $a1, $s0 #first - last + 1

   
#once more pushing onto stack
   
addi $sp, $sp, -4 #adjusting stack pointer
sw $0, 0($sp)	  #saving stack pointer

addi $sp, $sp, -4 #adjusting stack pointer
sw $a1, 0($sp)	  #saving a1 

             

jal getnum # go to the function getnum
move $s3, $v0 	#s3 has return value

#popping!!! we are doing the ecludean algorithm here
lw $a1, 0($sp) 		#restore in a1
addi $sp, $sp, 4	#adjust stack pointer
lw $a0, 0($sp)		#restre in a0

addi $sp, $sp, 4	#adjust stack pointer
divu $s3, $s1 		#dividing
mfhi $s4 		#s4 now has remainder
addu $v0, $s4, $a0 	# adding it to the last

             
lw $ra, 0($sp) 		#restore ra
addi $sp, $sp, 4	#adjust stack pointer

lw $s3, 0($sp) 		#restore s3
addi $sp, $sp, 4	#adjust stack pointer

lw $s2, 0($sp) 		# restore s2 (i)
addi $sp, $sp, 4	#adjust stack pointer

lw $s1, 0($sp) 		#restore s1 (10000)
addi $sp, $sp, 4	#adjust stack pointer


lw $s0, 0($sp) 		#restore s0 (1)
addi $sp, $sp, 4	#adjust stack pointer
jr $ra 			#jump back pc + 4

             

#this part of mips is the same as get_random in c
#multiplying by the constant 36969 and adding m_z >> 16
# we do this both for m_z and m_w which will get a 32 bit result
#thereafter we return result
#for mips im using stacks for saving the numbers i need
getnum:
#pushing values onto stack
addi $sp, $sp, -4	#adjust stack pointer
sw $s0, 0($sp)		#storing value in s0

addi $sp, $sp, -4	#adjust stack pointer
sw $ra, 0($sp)		#storing value into ra

lw $t0, m_z		#restore t0
li $t1, 36969		#int t1 = 36969
andi $t2, $t0, 65535	

mul $t2, $t1, $t2	#multiplying the integers
srl $t1, $t0, 16	#shifting right	
addu $t0, $t2, $t1	#adding unsigned

sw $t0, m_z		#saving into t0
lw $t1, m_w		# restoring m_z into t1
li $t2, 18000		#load intger
andi $t3, $t1, 65535	#adding t1 + int
mul $t3, $t2, $t3	#multipying integers
srl $t2, $t1, 16	#shifting right logic	
addu $t1, $t3, $t2	#binary addition algorithm
sw $t1, m_w		#save word into t1

             

sll $t0, $t0, 16	#shifting once more
addu $t0, $t0, $t1	#adding unsigned
move $v0, $t0		#move t0 into v0

             

lw $ra, 0($sp)		#load into ra
addi $sp, $sp, 4	#adjusting stck pointer
lw $s0, 0($sp)		#load into s0
addi $sp, $sp, 4	#adjust stack pointer
jr $ra			#jump back to pc + 4

 
.data
mssg1: .asciiz "\n\nGreatest Common Denominator of "
mssg2: .asciiz " AND "
mssg3: .asciiz " = "

m_z: .word 60000 # uint32_t m_z = 60000
m_w: .word 50000 # uint32_t m_w = 50000
