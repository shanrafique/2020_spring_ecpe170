#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

struct clues{

  char words[256];
  char clue[256];
  int start_row;
  int start_column;
  char horOrVert[1];
  int maybefound;  //found = 1 !found = 0
};
