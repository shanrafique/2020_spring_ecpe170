//Shan Rafique, s_rafique@u.pacifc.edu 

#include "crosswordheader.h"
int main(void) {

//declaring the variables 
    struct clues *puzzle; //array for the structure
    int num_rows;
    int num_cols;
    int num_clues; 
    int userInput;
    int counter;  
    char userSolution[256]; 
    char alphaUserSolution[256]; 
    char file[256]; 
     

//file open place data in file into variables
    FILE *pointer_file; 
    char buf[1000]; 

    printf("Please enter file you wish to play: "); 
    scanf("%s",file); 
    pointer_file = fopen(file, "r");
    if(!pointer_file)
      return 1; 

    //scans first line of file
    fscanf(pointer_file, "%i %i %i", &num_rows, &num_cols,&num_clues);
  //  printf("%i %i %i",num_rows,num_cols,num_clues); 

    puzzle = (struct clues *)malloc(sizeof(struct clues) * num_clues);


    //[^\n]s reads strings until finds \n 
  for (int i =0; i < num_clues; i++){
      fscanf(pointer_file, "\n%c %i %i %s %[^\n]s", puzzle[i].horOrVert,&puzzle[i].start_row, &puzzle[i].start_column,puzzle[i].words,puzzle[i].clue);

      //printf("\n%s %i %i %s %s", puzzle[i].horOrVert,puzzle[i].start_row, puzzle[i].start_column, puzzle[i].words, puzzle[i].clue); 
  }


    
    //Creating the 2-D array 
    char **array2D;
    array2D = (char **)malloc(sizeof(char *)*num_rows); 
    for(int j =0; j < num_cols;j++){
      array2D[j] = (char *)malloc(sizeof(char *)*num_cols); 
    }


    //for loop to make the start_row & start_column in 2D array = #
    for(int i = 0; i < num_rows; i++){
        for(int j = 0; j < num_cols ; j++){
            array2D[i][j] = '#'; 
        }
      }

    
    //make everything in 2D array have '_' where a word will be
    for(int i = 0; i < (num_cols + 1); i++){
  
          int result = strcmp(puzzle[i].horOrVert, "H");         
          if(result == 0){
            //this is horizontal 
          for(int j = 0; j < strlen(puzzle[i].words) ; j++){ 
           array2D[(puzzle[i].start_row)-1][((puzzle[i].start_column -1)) + j] = '_'; 
          }
          }
          else{
            //this is vertical
          for(int j = 0; j < strlen(puzzle[i].words) ; j++){ 
          array2D[((puzzle[i].start_row)-1) + j][(puzzle[i].start_column -1)] = '_'; 
          }
          }
          }

      

  
    //if user inputs -1 it will stop
    while(userInput != -1){
      //prints top numbers
      printf("\n");
      for(int i=0;i < num_rows;i++){
        if(i == 0)
          printf("\t\t%i", i+1);
        else
          printf("\t%i", i+1);
      }

      //prints ---- on top 
      printf("\n");
       for(int i=0;i < num_rows;i++){
        if(i == 0)
          printf("\t------");
        else
          printf("----");
      
      }

      //prints array2D
      for(int i = 0; i < num_rows; i++){
                printf("\n%i\t| ", i+1); 
                for(int j = 0; j < (num_cols + 1) ; j++){
                    printf("\t%c ",array2D[i][j]); 
                }
      }

    //prints what the user can see
      printf("\n\n"); 
      printf("# Direct  Row/Col \tClue");
      

      for(int i=0;i < (num_clues);i++){
        if(puzzle[i].maybefound == 0){

          printf("\n%i:\t%s \t\t%i/%i  %s", i+1, puzzle[i].horOrVert,puzzle[i].start_row,puzzle[i].start_column,puzzle[i].clue); 
        }
      }

      printf("\n\nEnter clue to solve or -1 to exit: ");
      scanf("%i", &userInput);

      if(userInput != -1){
      printf("\nEnter your solution: ");
      scanf("%s", userSolution);


    
    //char alphaUserSolution = toupper(userSolution); 
    //make the user input into upper case
    for (int i =0; i < (sizeof(userSolution)); i++){
        alphaUserSolution[i] = toupper(userSolution[i]); 
    }
  

    //Checking if user put in correct word if yes change board if no then no change

      // printf("%s",puzzle[0].words);       
      int check = strcmp(puzzle[userInput -1].words, alphaUserSolution);
      if(check == 0){
        int result = strcmp(puzzle[userInput -1].horOrVert, "H");
        if(result == 0){
          //this is horizontal
         for(int i =0; i < (strlen(puzzle[userInput -1].words)); i++){
      
            array2D[(puzzle[userInput -1].start_row)-1][(puzzle[userInput -1].start_column -1) + i] = puzzle[userInput -1].words[i];   
         }
        counter++;
        puzzle[userInput -1].maybefound = 1; 
      }
        else{
          
           //this is verticle
         for(int i =0; i < (strlen(puzzle[userInput -1].words)); i++){
      
            array2D[((puzzle[userInput -1].start_row) -1) + i] [(puzzle[userInput -1].start_column -1)] = puzzle[userInput -1].words[i];   
         }
              counter++; 
              puzzle[userInput -1].maybefound = 1; 

        }
      }
      else{
        printf("Ha! Wrong answer. Try again.");
      }
      
      if(counter == num_clues){
        printf("CONGRATULATION YOU WON!");
        userInput = -1;  
      }
      
    }

    

    }
    
  

  //Freeing the memory 
  for(int i=0; i < num_clues;i++){
      free(puzzle[i].words);
      free(puzzle[i].clue);
      free(&puzzle[i].start_row);
      free(&puzzle[i].start_column);
      free(&puzzle[i].maybefound);
    }


    for(int i =0; i < num_rows;i++){
      for(int j =0; j < num_cols; j++){
          free(&array2D[i][j]); 

      }
    }

    fclose(pointer_file);


  return 0;
    }
