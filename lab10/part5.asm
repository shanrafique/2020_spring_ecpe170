.globl main
.text


#s0-s7
#t0-t9


# map 
#s7 --> result
#s0 ---> users input
#t0 --> temp var to hold location of user input
#Displayed the user should enter the word 
#int main()
#{
#  char string[256];
# int i=0;
#char *result = NULL;  // NULL pointer is binary zero
  
#  // Obtain string from user, e.g. "Constantinople"
#  scanf("%255s", string); 
  
#  // Search string for letter 'e'.
#  // Result is pointer to first e (if it exists)
#  // or NULL pointer if it does not exist
#  while(string[i] != '\0') {
#    if(string[i] == 'e') {
#      result = &string[i]; 
#      break; // exit from while loop early
#    }
#    i++;
#  }

#  if(result != NULL) {
#    printf("First match at address %d\n", result);
#    printf("The matching character is %c\n", *result);
#  }
#  else
#    printf("No match found\n");
#}


main:

la $s0, userInput	# s0 = address userinput

li $s7, 0		# s7 = address of result

li $s1, 0		# i (s1) = 0 

li $t9, 'e'	# will store ascii numer for e
li $t8 '\n'	# will store ascii number for \n

#displays the message 
li $v0, 4
la $a0, message
syscall

#Get user input
li $v0, 8
la $a0, userInput
li $a1, 256
syscall


#keeps iterating till find e or new line character

while:

add $s0, $s0, $s1	# i + address of userinput
lb $t0, 0($s0)	# will store ascii number for 1st letter (s1 = i)

beq $t0, $t8, if2nd	# if letter == \n

beq $t0, $t9, yesE	#if 1st letter in word == e 


addi $s1, 1		#char is 1 byte so + 1 to the address of userInput

j while


#if the letter does == e then we give s7 the asscii

yesE:
lb $s7, 0($s0)	#give result the address of e
j if2nd

#the seconf if statemetn from the code

if2nd:
beq $s7, $zero, nomatch

#displays the message 
li $v0, 4
la $a0, found
syscall

j end

nomatch:
#displays the message 
li $v0, 4
la $a0, fail
syscall
j end

end:
li $v0, 10
syscall


.data
found: .asciiz "Found e in word congratz"
fail: .asciiz "No e found in word"
message: .asciiz "Please input a word: "
userInput: .space 256  	#array with 256 memory
result: .word 0		#array with 256 spots
