.globl main
.text



#s0-s7
#t0-t9



#int main()
#{
#   int A[5]; // Empty memory region for 5 elements
#   int B[5] = {1,2,3,4,5};
#   int C=12;
#   int i;

#   for(i=0; i<5; i++)
#   {
#      A[i] = B[i] + C;
#   } 

#   i--;
#   while(i >= 0)
#   {
#      A[i]=A[i]*2;
#      i--;
#   } 
#}


#s0 address of arraya
#s1 address of arrayb
# s2 was just 12
# s3 i for incrementation


#in this main we make the array that had variables attain new variables values
main:

la $s0, arrayA	# s0 = &arrayA address
la $s1, arrayB	# s1 = &arrayB

li $s2, 12	# s2 (c) = 12
li $s3, 0	# s3 (i) = 0

li $t0, 5	# t0 = 5 (i < 5)


# for loop thay will exit when i becomes greater or equal to 5

forloop:

blt $s3, $t0, insideFor	# i (s3) < 5 (t0)

addi $s3, $s3, -1	# i = i - 1

j whileloop


# if the foor loop iterates this is the insides of the for loop
insideFor:

add $t1, $s3, $s3	# i + i (2*i)
add $t1, $t1, $t1	# (i + i) --> t1  + i (s3)   (4*i)  


add $t2, $s1, $t1	# t2 = &arrayB at whtever i is
add $t3, $s0, $t1	# t3 = &arrayA at whatever i is

lw $t4, 0($t2)		# t4 = t2 (arrayB whateever i is)

add $t5, $t4, $s2	# t5 = arrayB whatever i is + C

sw $t5, 0($t3)		# t5 value is now in t3 (&arrayA at whtever i is)


addi $s3, $s3, 1	#i = i + 1
j forloop


#after all the for loop now we come to a while which will iterate as long as i is greater than 0 

whileloop:

bge $s3, $zero, multiply2

li $v0, 10
syscall

multiply2:

add $t1, $s3, $s3	# 2*i
add $t1, $t1, $t1	# i + i + i + i (4 * i)

add $t6, $s0, $t1	# address of arrayA (s0) + offset (t1)

lw $t7, 0($t6)		#load value of top into t7

add $t8, $t7, $t7	# a[i] + a[i] (a[i] * 2)

sw $t8, 0($t6)		# save input into array at specific address i

addi $s3, $s3, -1
j whileloop





#here are my 2 arrays in memory

.data
arrayA: .space 20
arrayB: .word 1, 2, 3, 4, 5
