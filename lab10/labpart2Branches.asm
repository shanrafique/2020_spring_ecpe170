.globl main
.text



# s0 - s7
# t0 - t9



#maps
# s0 a
#s1 B
#s2 c
#s3 z 


#   if(A > B || C < 5)
#      Z = 1;
#   else if((A > B) && ((C+1) == 7))
#      Z = 2;
#   else
#      Z = 3;

 #  switch(Z)
  #  {
 #     case 1:
 #        Z = -1;
 #        break;
 #     case 2:
 #        Z -=-2;
 #        break;
 #     default:
 #        Z = 0;
 #       break;
 #   }
#}

#temporary vairbales are for the switch statment 

#int main()
#{
#   // Note: I should be able to change
#   // the values of A, B, and C when testing
#   // your code, and get correct output each time!
#   // (i.e. don't just hardwire your output)
#   int A=10;
#   int B=15;
#   int C=6;
#   int Z=0;

main:

lw $s0, A	#A = 10
lw $s1, B	#B = 15
lw $s2, C	#C = 16
lw $s3, Z	#Z = 0


li $t0, 5	# temp = 5
li $t1, 7	# temp = 7

#temps for switch
li $t2, 1	#temp2 = 1
li $t3, 2	#temp3 = 2
li $t4, 3	#temp4 = 3

 


bgt $s0, $s1, zequal1		# a > b
blt $s2, $t0, zequal1		#c < 5


bgt $s0, $s1, nextif		#A > B if yes jump to nextif (its an &&)

li $s3, 3		#else z = 3
j switch		#DONT FROGET JUMP HERE!

zequal1:
li $s3, 1	# z = 1
j switch


zequal2:
li $s3, 2		# z = 2
j switch


nextif:
addi $s2, $s2, 1	#c = c + 1
beq $s2, $t1, zequal2   # c + 1 == 7? if yes jump


switch:

beq $s3, $t2, equalneg1		#if z = 1
beq $s3, $t3, minus2 		#if z = 2

li $s3, 0
j rof


#if it equals to negative 1 it goes here and place it with a -1 value
equalneg1:
li $s3, -1	# z = 1
#sw $s3, Z	# putting the value s3 into Z (dont need i suppose) 
j rof


#will subtract 2 from the registra's s3
minus2:
sub $s3, $s3, $t3	# z = z - 2
j rof

#ends the program
rof:
li $v0, 10
syscall


.data
A: .word 10
B: .word 15
C: .word 6
Z: .word 0







