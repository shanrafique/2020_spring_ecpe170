.globl main
.text


#s0-s7
#t0-t9




#int main()
#{
#   int Z=2;
#   int i;

#   i=0;
#   while(1){
#     if(i>20)
#       break;
#    Z++;
#    i+=2;
#   }
 
#   do {
#      Z++;
#   } while (Z<100);
  
#   while(i > 0) {
#      Z--;
#      i--;
#   }
#return 0;
#}



#maps
# S0 i for incrementation
# s1 z
# t0 20
# t1 100
#
#here in main we do a while loop intil i increments greater than 20
main:

lw $s0, i	# i is in s0 
lw $s1, z	# z is in s1

li $t0, 20	# t0 = 20 
li $t1, 100	# t1 = 100



while:

ble $s0, $t0, increment		#i = 0 <= t0 (20)
j gotobreak


#this is where i increments itself
increment:
addi $s1, $s1, 1	# z = z + 1

addi $s0, $s0, 2	# i = i + 2

j while


#if z is not less than 100 it will go to 2nd while loop
gotobreak:
blt $s1, $t1, incrementZ	# z < 100 go to incrementZ

j whileloop2


#here we have an incrementation of z
incrementZ:
addi $s1, $s1, 1	#z = z + 1
j gotobreak

whileloop2:

bgt $s0, $zero, decrement


li $v0, 10
syscall


#here we have an decrmeentation of i variable

decrement:
addi $s1,$s1,-1		# z = z - 1
addi $s0, $s0, -1	# i = i -1
j whileloop2


#the variables we saved
.data
i: .word 0	# 32 bit integers with i = 0 , z = 2
z: .word 2
