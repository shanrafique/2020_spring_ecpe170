.globl main
.text



# s0 - s7
# t0 - t9


#maps
# s0 A
# S1 B
# S2 C
# S3 D
# s4 E
# s5 F
# z is under data 

#in this mips code we do some arithmetic calculation 
main:

li $s0, 15
li $s1, 10
li $s2, 7
li $s3, 2
li $s4, 18
li $s5, -3

la $s6, Z


add $t0, $s0,$s1 	#A + B
sub $t1, $s2,$s3	# C - D
add $t2, $s4,$s5	# E + F 
sub $t3, $s0,$s2	# A - C

add $s0, $t0, $t1  # (A + B) + (C-D)
sub $s1, $t2, $t3 #  (E+F) - (A-C)

add $s3, $s0, $s1

sw $s3, 0($s6)

#Z


li $v0, 10
syscall


.data

Z: .word 0




